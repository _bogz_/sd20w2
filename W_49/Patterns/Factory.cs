using System;
namespace Patterns
{

    public interface IFactory
    {
        void Drive(int miles);
    }


    public class Car : IFactory
    {
        public void Drive(int miles)
        {
            Console.WriteLine("Drive the Car : " + miles.ToString() + "km");
        }
    }

    public class Bike : IFactory
    {
        public void Drive(int miles)
        {
            Console.WriteLine("Drive the Bike : " + miles.ToString() + "km");
        }
    }

    public class Truck : IFactory
    {
        public void Drive(int miles)
        {
            Console.WriteLine("Drive the Truck : " + miles.ToString() + "km");
            Console.WriteLine("Chiu chiu");
        }
    }


    public abstract class VehicleFactory
    {
        public abstract IFactory GetVehicle(string Vehicle);
    }


    public class ConcreteVehicleFactory : VehicleFactory
    {
        public override IFactory GetVehicle(string Vehicle)
        {
            switch (Vehicle)
            {
                case "Car":
                    return new Car();
                case "Bike":
                    return new Bike();
                case "Truck":
                    return new Truck();
                default:
                    throw new ApplicationException(string.Format("Vehicle '{0}' cannot be created", Vehicle));
            }
        }

    }
}
using System;

namespace Patterns
{
    public abstract class Something
    {
        public void SayHello(){
            Console.WriteLine("Hello");
        }

        public abstract bool IsMonday(bool flag);

        public virtual bool IsTuesday(bool flag){
            Console.WriteLine("Please override me");
            return flag;
        }
    }

public abstract class AnotherTest : Something
{
    public override abstract bool IsMonday(bool flag);
}


    public class SomethingElse : Something
    {
        public override bool IsMonday(bool flag)
        {
            Console.WriteLine($"Is monday: {flag}");
            return flag;
        }

        public override bool IsTuesday(bool flag)
        {
            Console.WriteLine("Hello world");
            return !flag;
        }
    }

    public class SomethingTrulyElse {
        public void SayMorning()
        {
            Console.WriteLine("Morning");
        }

        public virtual void SayGoodNight()
        {
            Console.WriteLine("Morning");
        }
    }
}
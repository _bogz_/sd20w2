﻿using System;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            try{
                // VehicleFactory factory = new ConcreteVehicleFactory();

                // IFactory car = factory.GetVehicle("Car");
                // car.Drive(10);

                // IFactory bike = factory.GetVehicle("Bike");
                // bike.Drive(20);

                // var sthElse = factory.GetVehicle("Truck");
                // sthElse.Drive(1245);

                // BikeShop.UpgradeBike();

                StrategyPattern.RunStrategyPattern();
            }
            catch(Exception){
                Console.WriteLine("The given vehicle type is not supported");
            }
           
        }
    }
}

const jwt = require("jsonwebtoken");

module.exports = (req, res) => {
    let auth_header = req.header('Authorization');
    console.log(auth_header);
    var card = jwt.verify(auth_header, 'tes123-secret');
    return res.status(200).send({"payload": card});
}
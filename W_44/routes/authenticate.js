const jwt = require("jsonwebtoken");

module.exports = (req, res) => {
    var card = jwt.sign(req.body, 'test123-secret');
    return res.status(200).send({"auth_token": card});
}
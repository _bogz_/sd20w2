const express = require("express");

const app = express();
app.use(express.json());


app.get('/', require("./routes/index.js"));
app.post('/auth/login', require("./routes/authenticate.js"));
app.get('/auth/verify-token', require("./routes/authorize.js"));


app.listen("3000", (err) =>{

    if(err){
        console.log("An error occured");
        return;
    }
    console.log("Listening on port 3000");
});
var ESB = require('light-esb-node');


function esbCallback(error, message){
    if(error)
    {
        console.log("Encountered an error: ", error);
    }
    else{
        console.log("Received: ", message);
        console.log("Processing...");
    }
}

var component = ESB.createLoggerComponent(esbCallback);
var external_component = ESB.createCallComponent(esbCallback, "https://jsonplaceholder.typicode.com/users", "get");
var receiver2 = ESB.createLoggerComponent(esbCallback);
var receiver3 = ESB.createLoggerComponent(esbCallback);

component.connect(external_component);
component.connect(receiver3);

external_component.connect(receiver2);

var json_message = {
    greeting: "Hello World"
};

var message = ESB.createMessage(json_message,"Bogdan", "CRM", "SomeId");

component.send(message);
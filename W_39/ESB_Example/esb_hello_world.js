var ESB = require('light-esb-node');

function esbCallback(error, message){
    if(error)
    {
        console.log("Encountered an error: ", error);
    }
    else{
        console.log("Received: ", message);
        console.log("Processing...");
    }
}

var component = ESB.createLoggerComponent(esbCallback);
var receiver1 = ESB.createLoggerComponent(esbCallback);
var receiver2 = ESB.createLoggerComponent(esbCallback);

component.connect(receiver1);
component.connect(receiver2);

var json_message = {
    greeting: "Hello World"
};

var message = ESB.createMessage(json_message,"Bogdan", "CRM", "SomeId");

component.send(message);
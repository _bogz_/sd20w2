var ESB = require('light-esb-node');
const sqlite3 = require('sqlite3');

var db = new sqlite3.Database('logging_db.sqlite');



function esbCallback(error, message){
    if(error)
    {
        console.log("Encountered an error: ", error);
    }
    else{
        console.log("Received: ", message);
        console.log("Processing...");
    }
}

var component = ESB.createLoggerComponent(esbCallback);
var receiver2 = ESB.createScriptComponent(esbCallback, (esbMessage, callback) => {
    console.log("Message is", esbMessage);
    db.run(`INSERT INTO logging_info VALUES (?,?,?,?)`, 
        [esbMessage.payload.greeting, esbMessage.context.caller.user, esbMessage.context.caller.system, esbMessage.context.caller.correlationId ], function(err) {
            if(err){
                return;
            }
            console.log('A record was inserted in the database');
        });
    db.close();
});

component.connect(receiver2);


var json_message = {
    greeting: "Hello World"
};

var message = ESB.createMessage(json_message,"Bogdan", "CRM", "SomeId");

component.send(message);
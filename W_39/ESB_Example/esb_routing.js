var ESB = require('light-esb-node');

// callback function
function esbCallback(error, message){
    if(error){
        console.log('Error while processing the message\n', error, message);
    }
    else{
        console.log('Message received...');
        console.log('Message: ', message);
        console.log('Processing')
    }
}

// entry component = this will simply log the payload 
let component = ESB.createLoggerComponent(esbCallback);

// route component. This creates channels that depending on the sender will do different things
let route_message = ESB.createRouteComponent(esbCallback, {
	routeItems: [
		{
			routeFunction: function(esbMessage){
				if(esbMessage.context.caller.user=="john")
					return true;
				return false;
			},
			channel: "john"
		},
		{
			routeFunction: function(esbMessage){
				if(esbMessage.context.caller.user=="marry")
					return true;
				return false;
			},
			channel: "marry"
		}
	]
});  

let john_component = ESB.createScriptComponent(esbCallback, function(esbMessage, callback){
    console.log("John");
    console.log("Message is", esbMessage);
});

let marry_component = ESB.createScriptComponent(esbCallback, function(esbMessage, callback){
    console.log("Marry");
    console.log("Message is", esbMessage);
});

let example_message = {
    user: 'test',
};

// The messages have to have the caller coresponding to the caller.user in this case...
let message_john = ESB.createMessage(example_message, "john", "CRM", "SomeID");
let message_marry = ESB.createMessage(example_message, "marry", "CRM", "SomeID");
let message = ESB.createMessage(example_message, "bogdan", "CRM", "SomeID");

component.connect(route_message);
route_message.connect("john", john_component);
route_message.connect("marry", marry_component);

// Simply sending the message to the main component that will then send it to the receiver1 and then receiver 2
component.send(message);
component.send(message_john);
component.send(message_marry);
CREATE TABLE logging_info (
    [Greeting] TEXT NOT NULL,
    [Sender] TEXT NOT NULL,
    [SenderSystem] TEXT NOT NULL,
    [UniqueIdentifier] TEXT NOT NULL
);
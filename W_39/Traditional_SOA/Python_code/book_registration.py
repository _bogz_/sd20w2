import sqlite3
import requests
import json

db = sqlite3.connect('library_example.sqlite')
url = 'http://localhost:8088/generate-isbn'
cursor = db.cursor()

# validate_input

def validate_input(inp):
    while inp.lower() != 'y' and inp.lower() != 'n':
        inp = input("Oops try again [y/n]: ")
    if inp.lower() == 'y':
        return True
    else:
        return False


# add_books
def add_books(title, author, year):
    response = requests.post(url, json={'title': title, 'author': author, 'year': year})
    data = response.json()
    print(data['ISBN'])

    sql_query = '''INSERT INTO books VALUES(?,?,?,?)'''
    cursor.execute(sql_query, [title, author, year, data['ISBN']])
    # cursor.execute(sql_query, [title, author, year, "Test"])
    db.commit()


print('Hello. This is an online library')
inp = input('Would you like to add a book? [y/n]: ')

while True:
    flag = validate_input(inp)
    if flag == True:
        title = input("Title :")
        author = input("Author: ")
        year = input("Year: ")
        add_books(title, author, year)
        inp = input('Would you like to add another book? [y/n]: ')
    else:
        print("Goodbye...")
        break
CREATE TABLE books (
    [Title] TEXT NOT NULL,
    [Author] TEXT NOT NULL,
    [Year] TEXT NOT NULL,
    [Isbn] TEXT NOT NULL
)
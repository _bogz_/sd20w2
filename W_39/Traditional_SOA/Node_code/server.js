const bodyParser = require('body-parser')
const express = require('express');


app = express();
app.use(bodyParser.json());

app.get('/test', (req, resp) => {
    resp.status(200).send({'message': "App is live and well"})
});


app.post('/generate-isbn', (req, res) => {
    let body = req.body;
    console.log(body);
    let isbn = {
        ISBN: `ISBN-1234-${req.body.title.charCodeAt(0)}-${req.body.year}`
    };
    res.status(200).send(isbn);

})






app.listen(8088, (err)=>{
    if(err){
        console.log(err);
        console.log("Oopps");
    }
    else{
        console.log("App is running on 8088...");
    }
});
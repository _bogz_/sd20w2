using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using UsersApi.Models;
using UsersApi.Services;

namespace UserApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserRepository _repo;


        public UsersController(IUserRepository repo)
            => _repo = repo; 

    
        
        /// <summary>Create User</summary>
        /// <param name="createUserRequest">Model</param>
        [HttpPost]
        [ProducesResponseType(typeof(User), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<User>> CreatePerson([FromBody, BindRequired] CreateUserRequest createPersonRequest)
        {
            try
            {
                var person = await _repo.CreateUserAsync(createPersonRequest);
                return Created(string.Empty, person);
            }
            catch(ArgumentException e){
                return BadRequest(e);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get all Users</summary>
        [HttpGet]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<User>> GetAllUsers()
        {
            try
            {
                var users = await _repo.GetAllAsync();
                return Ok(users);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }


        /// <summary>Get User by id</summary>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<User>> GetUser([FromRoute, BindRequired]int id)
        {
            try
            {
                var user = await _repo.GetUserAsync(id);
                if(user == null)
                    throw new NullReferenceException();
                return Ok(user);
            }
            catch(NullReferenceException e){
                return NotFound(e);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
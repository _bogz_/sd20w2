using Microsoft.EntityFrameworkCore;

namespace UsersApi.Models
{

    public class UsersDbContext : DbContext
    {
        public DbSet<User> Users{get;set;}

        protected override void OnConfiguring(DbContextOptionsBuilder options)
           => options.UseSqlite("Data Source=sqlite_user.sqlite");
    
    }
}
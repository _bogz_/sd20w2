using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UsersApi.Models;
using UsersApi.Services;

namespace UserApi.Services
{
    public class UserRepository : IUserRepository
    {
        private UsersDbContext _dbContext;

        public UserRepository(UsersDbContext dbContext)
            => _dbContext = dbContext;

        public async Task<User> CreateUserAsync(CreateUserRequest request)
        {
            if(string.IsNullOrEmpty(request.Name) || string.IsNullOrEmpty(request.Email))
                throw new ArgumentException("You have to supply a name and email...");

            var user = new User{
                Name = request.Name,
                Email = request.Email
            };

            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();

            return user;
        }

        public async Task<List<User>> GetAllAsync()
            => await _dbContext.Users.ToListAsync();

        public async Task<User> GetUserAsync(int id)
            => await _dbContext.Users.FirstOrDefaultAsync(_ => _.Id == id);
    }
}
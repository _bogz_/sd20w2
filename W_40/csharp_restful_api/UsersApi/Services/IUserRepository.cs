using System.Collections.Generic;
using System.Threading.Tasks;
using UsersApi.Models;

namespace UsersApi.Services
{
    public interface IUserRepository
    {
        Task<User> CreateUserAsync(CreateUserRequest request);
        Task<User> GetUserAsync(int id);
        Task<List<User>> GetAllAsync();
    }
}
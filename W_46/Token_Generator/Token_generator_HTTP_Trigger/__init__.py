import logging

import azure.functions as func
from jose import jwt
from datetime import datetime, timedelta
import json


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('A new request has been received...')
    logging.info('Processing')

    try:
        req_body = req.get_json()
    except ValueError:
        pass
    else:
        nemid = req_body.get('nemid')
        code = req_body.get('generatedCode')
        print(nemid)

    if nemid and code:
        if validate_request(nemid, code):
            generated_token = generate_token(nemid)
            return func.HttpResponse(json.dumps({"token": generated_token}), status_code=200)
        else:
            return func.HttpResponse(json.dumps({"message": "Could not find an authentication request with the given credentials"}),
            status_code=403)
    else:
        return func.HttpResponse(
             "The request did not contain a nemid and a generated code",
             status_code=400
        )


def generate_token(nemid):
    payload = {
        "nemid": nemid,
        "permissions": "basic-user",
        "iat":datetime.now(),
        "exp": datetime.now() + timedelta(minutes=20)
    }

    token = jwt.encode(payload, "this is a secret", algorithm="HS256")
    return token


def validate_request(nemid, generatedCode):
    # Connect to the database
    # Check if the auth log exists and if the request is older than 20 minutes
    # Based on that return things 

    storage = [
        {
            "nemid": 1234567,
            "generatedCode": 123987
        },
        {
            "nemid": 1231823731,
            "generatedCode": 123151
        },
        {
            "nemid": 12123412,
            "generatedCode": 1231231
        },
        {
            "nemid": 12312312123123,
            "generatedCode": 998987
        },
        {
            "nemid": 981239828,
            "generatedCode": 0000000
        }
    ]

    for entry in storage:
        if entry["nemid"] == nemid and entry["generatedCode"] == generatedCode:
            return True
    
    return False

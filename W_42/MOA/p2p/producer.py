import pika
import json
import time

conection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

channel = conection.channel()

channel.queue_declare(queue="sd20w2")

a = int(input("Number a >"))
b = int(input("Number b >"))


message = {"a": a, "b": b}

channel.basic_publish(
    exchange='',
    routing_key="sd20w2",
    body=json.dumps(message),
    properties=pika.BasicProperties(delivery_mode=2)
)

print("Done...")
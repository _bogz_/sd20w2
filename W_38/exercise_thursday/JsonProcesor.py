from abc import ABCMeta, abstractmethod
from IFileProcesor import IFileProcessor
import json

class JsonProcessor(IFileProcessor):
    @classmethod
    def read_file(self, file_name):
        with open(file_name, "r") as json_f:
            data = json.load(json_f)
            return data


    @classmethod
    def write_file(self, file_name, content):
        with open(file_name,"w") as json_f:
            json.dump(content, json_f)

    
    @classmethod 
    def serialize_store(self, file_name, content):
        with open(file_name, 'w') as f_json:
            json.dump(content, f_json)

    @classmethod
    def desirialize(self, file_name):
        print("To be implemented")
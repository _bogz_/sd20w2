import json

with open('person.json', 'w') as f:
    json_payload = json.dumps({'name':'Something', 'age': 73, 'email':'sth@sth.com'})
    f.write(json_payload)


with open('person.json', 'r') as json_file:
    data = json_file.readlines()
    print(data)
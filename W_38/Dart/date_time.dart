main(List<String> args) {
  var dateTime = DateTime.tryParse(args[0]) ?? DateTime.now();
  print('DateTime: $dateTime');
  print('To ISO8601: ${dateTime.toIso8601String()}');
  print('To UTC: ${dateTime.toUtc()}');
}

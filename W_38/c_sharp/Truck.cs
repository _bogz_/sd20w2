using System;

namespace c_sharp
{
    public class Truck : IVehicle
    {
        public string Brand {get;set;}  
        private int Weight {get;set;}
        private bool IsRunning{get;set;}
        private int KillometersDriven{get;set;}

        public int drive(int km)
        {
            if(!IsRunning){
                Console.WriteLine("Start your truck, dummy.");
            }

            KillometersDriven += km;
            Console.WriteLine("Vroom");
            return km;
        }

        public void start_engine()
        {
            if(IsRunning)
            {
                Console.WriteLine("Truck is running already");
                return;
            }
            IsRunning = true;
            Console.WriteLine("Truck is running");
        }

        public void stop_engine()
        {
            if(IsRunning)
            {
                IsRunning = false;
                Console.WriteLine("Truck is stopped");
                return;
            }
            Console.WriteLine("Truck is stopped already");
        }

        public int DrivenKillometers()
            => KillometersDriven;

        public void LoadTruck(int weight){
            Weight += weight;
            Console.WriteLine($"The truck is loaded with {Weight}");
            }
        
        public void UnloadTruck()
            => Weight = 0;
    }
}
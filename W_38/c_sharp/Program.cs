﻿using System;

namespace c_sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello SD20w2!");

            var car = new Car{
                Brand = "Brand",
                FabricationYear = "2020"
            };

            car.drive(12);
            car.start_engine();
            car.drive(200);
            car.stop_engine();


            var truck = new Truck{
                Brand = "Brand"
            };

            truck.drive(12);
            truck.start_engine();
            truck.drive(200);
            truck.stop_engine();
            truck.LoadTruck(1000);
            truck.UnloadTruck();
        }
    }
}

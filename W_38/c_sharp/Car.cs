using System;

namespace c_sharp
{
    public class Car : IVehicle
    {
        public string Brand {get;set;}
        public string FabricationYear{get;set;}

        private bool IsRunning{get;set;}
        private int KillometersDriven{get;set;}

        public int drive(int km)
        {
            if(!IsRunning){
                Console.WriteLine("Start your car, dummy.");
            }

            KillometersDriven += km;
            Console.WriteLine("Vroom");
            return km;
        }

        public void start_engine()
        {
            if(IsRunning)
            {
                Console.WriteLine("Car is running already");
                return;
            }
            IsRunning = true;
            Console.WriteLine("Car is running");
        }

        public void stop_engine()
        {
            if(IsRunning)
            {
                IsRunning = false;
                Console.WriteLine("Car is stopped");
                return;
            }
            Console.WriteLine("Car is stopped already");
        }

        public int DrivenKillometers()
            => KillometersDriven;
    }
}
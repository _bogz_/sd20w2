namespace c_sharp
{
    public interface IVehicle
    {
        int drive(int km);
        void start_engine();
        void stop_engine();
    }
}
import sqlite3


db = sqlite3.connect('people.sqlite')

# Read people from DB
db_cursor = db.cursor()
query = """SELECT * FROM person"""

for row in db_cursor.execute(query):
    print(f'ID: {row[0]}, Name: {row[1]}, Email: {row[2]}, Address: {row[3]}, Phone: {row[4]}')

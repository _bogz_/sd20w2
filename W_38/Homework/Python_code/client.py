import json


# name, email, address, phone

name = input("Name ")
email = input("Email ")
address = input("Address ")
phone = input("Phone ")

# store it in a json file

person = {
    "name": name,
    "email": email,
    "address": address,
    "phone": phone
}

with open("people.json", "w") as json_file:
    json.dump(person, json_file)
 
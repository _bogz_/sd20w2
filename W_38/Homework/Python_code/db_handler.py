import sqlite3
import json

db = sqlite3.connect('people.sqlite')

# Read the data from the file

with open("people.json", "r") as json_file:
    person = json.load(json_file)
    print(person['name'])

    # Make a query 
    db_cursor = db.cursor()
    query = """INSERT INTO person(Name, Email, Address, Phone) VALUES(?,?,?,?)"""
    person_array = [person['name'], person['email'], person['address'], person['phone']]
    db_cursor.execute(query, person_array)
    db.commit()


# execute it
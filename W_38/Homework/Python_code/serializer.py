import sqlite3
import pickle

db = sqlite3.connect('people.sqlite')

# Read people from DB
db_cursor = db.cursor()
query = """SELECT * FROM person"""

people = []

for row in db_cursor.execute(query):
    print(f'ID: {row[0]}, Name: {row[1]}, Email: {row[2]}, Address: {row[3]}, Phone: {row[4]}')
    people.append({
        "name": row[1],
        "email": row[2],
        "address": row[3],
        "phone": row[4]
    })

with open("pickled_people.p", "wb") as f:
    pickle.dump(people, f)

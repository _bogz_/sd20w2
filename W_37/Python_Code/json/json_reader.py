import json

with open('people.json', 'r') as json_file:
    data = json.load(json_file)
    for p in data['people']:
        print("Name " + p['name'])
        print("Age " + p['age'])
        print("Email " + p['email'])
    print("There are exactly " + str(data['count']) + ' people in your list')
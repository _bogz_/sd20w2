import json

def add_person():
    answer = input('Add a person? [Y/n]: ')
    while answer.lower() != 'y' and answer.lower() != 'n':
        answer = input('Try again... Add a person? [Y/n]: ')
    
    if answer.lower() == 'y':
        return True
    else: 
        return False


add_another = add_person()

people = {}
people['people'] = []
people['count'] = 0
ct = 0

while add_another:
    x = input('Name: ')
    y = input('Age: ')
    z = input('Email: ')

    ct += 1

    people['people'].append({
        'name': x,
        'age': y,
        'email': z
    })
    people['count'] = ct
    add_another = add_person()

with open('people.json', 'a+') as outfile:
    json.dump(people, outfile)



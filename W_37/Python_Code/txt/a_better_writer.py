
def add_new_person():
    name = input('Name: ')
    email = input('Email: ')
    phone = input('Phone: ')
    with open('people.txt', 'a+') as f:
        f.write(name + '\n' + email + '\n' + phone + '\n')


def validate_input(inp):
    while inp.lower() != 'y' and inp.lower() != 'n':
        inp = input('Try again[y/n]: ')
    
    if inp.lower() == 'y':
        return True
    else:
        return False


inp = input('Would you like to add a person? [y/n]: ')

while True:
    flag = validate_input(inp)
    if flag == True:
        add_new_person()
        inp = input("Add another one?")
    else:
        break

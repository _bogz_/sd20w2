from xml.dom import minidom

document = minidom.parse('data.xml')
cars = document.getElementsByTagName('car')

for elem in cars:
    print("Yet another car...\n")
    print("Brand: " + elem.firstChild.data + "\nColor: " + elem.attributes['color'].value 
    + "\nWeight: " + elem.attributes['weight'].value)


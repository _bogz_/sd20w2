import pandas as pd
df = pd.read_csv("csv_ex.csv")

names, lastnames, notes = "","",""

for index, row in df.iterrows():
    names += str(row['Name']) + ", "
    lastnames += str(row['Surname']) + ", "
    notes += '\n' + str(row['Note'])

print("Hi my names are", names)
print("Hi my last are", lastnames)
print("The notes are:", notes)
import pandas as pd
 
df = pd.DataFrame({'name': ['Raphael', 'Something'],
                    'age': [19, 22],
                    'note': ['big', 'green']})

df.to_csv('people_write.csv', index=False)